package com.theappchefs.ClinicaAvaria.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class CAReopSession extends RealmObject  {


    private String userId;


    private String token;

    public CAReopSession(){

    }
    static CAReopSession instance = null;

    public static CAReopSession getInstance(){
        if (instance == null){
            instance = new CAReopSession();
        }

        return instance;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
