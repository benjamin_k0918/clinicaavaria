package com.theappchefs.ClinicaAvaria.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

/**
 * Created by Cyhon on 3/3/2018.
 */

public class CardViewModel {

    int actual_price;
    String category;
    String code;
    String description;
    int discount_price;
    @ServerTimestamp
    Date final_date;
    String id;
    String image_url;
    String label;


    @ServerTimestamp
    Date start_date;
    int times_used;
    String title;
    String type;
    int usage_count;
    int usage_limit;
    int usage_per_customer;

    public int getActual_price() {
        return actual_price;
    }

    public String getCategory() {
        return category;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public int getDiscount_price() {
        return discount_price;
    }

    public Date getFinal_date() {
        return final_date;
    }

    public String getId() {
        return id;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getLabel() {
        return label;
    }

    public Date getStart_date() {
        return start_date;
    }

    public int getTimes_used() {
        return times_used;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public int getUsage_count() {
        return usage_count;
    }

    public int getUsage_limit() {
        return usage_limit;
    }

    public int getUsage_per_customer() {
        return usage_per_customer;
    }

    public void setActual_price(int actual_price) {
        this.actual_price = actual_price;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDiscount_price(int discount_price) {
        this.discount_price = discount_price;
    }

    public void setFinal_date(Date final_date) {
        this.final_date = final_date;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public void setTimes_used(int times_used) {
        this.times_used = times_used;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUsage_count(int usage_count) {
        this.usage_count = usage_count;
    }

    public void setUsage_limit(int usage_limit) {
        this.usage_limit = usage_limit;
    }

    public void setUsage_per_customer(int usage_per_customer) {
        this.usage_per_customer = usage_per_customer;
    }

}
