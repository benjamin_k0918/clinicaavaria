package com.theappchefs.ClinicaAvaria.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class CARepoUser extends RealmObject  {

    @Required
    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private String rut = null;
    private boolean isPreferedCustomer = false;

    static CARepoUser instance = null;

    public CARepoUser(){

    }

    public static CARepoUser getInstance(){
        if (instance == null){
            instance = new CARepoUser();
        }

        return instance;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getRut() {
        return rut;
    }

    public boolean isPreferedCustomer() {
        return isPreferedCustomer;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public void setPreferedCustomer(boolean preferedCustomer) {
        isPreferedCustomer = preferedCustomer;
    }

    public static void setInstance(CARepoUser instance) {
        CARepoUser.instance = instance;
    }
}
