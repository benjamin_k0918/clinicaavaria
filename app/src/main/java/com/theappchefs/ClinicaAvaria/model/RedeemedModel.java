package com.theappchefs.ClinicaAvaria.model;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by cyhon on 6/1/2018.
 */

public class RedeemedModel extends RealmObject {
    String id;
    String promotionId;
    String userId;
    Date date;


    public RedeemedModel() {
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public String getUserId() {
        return userId;
    }

    public Date getDate() {
        return date;
    }
}
