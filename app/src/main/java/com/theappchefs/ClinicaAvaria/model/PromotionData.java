package com.theappchefs.ClinicaAvaria.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by cyhon on 5/31/2018.
 */

public class PromotionData extends RealmObject{

    private double actual_price;
    private  String category;
    private  String code;
    private String description;
    private double discount_price;
    @ServerTimestamp
    private  Date final_date;
    private  String id;
    private  String image_url;
    private  String label;

    @ServerTimestamp
    private Date start_date;
    private  double times_used;
    private  String title;
    private  String type;
    private  double usage_count;
    private  double usage_limit;
    private double usage_per_customer;
    private  boolean isUserInterested = false;

    public PromotionData() {
    }

    public  PromotionData(PromotionModel promotionModel) {
       this.setActual_price(promotionModel.actual_price);
       this.setCategory(promotionModel.category);
       this.setCode(promotionModel.code);
       this.setDescription(promotionModel.description);
       this.setDiscount_price(promotionModel.discount_price);
       this.setFinal_date(promotionModel.final_date);
       this.setId(promotionModel.id);
       this.setImage_url(promotionModel.image_url);
       this.setStart_date(promotionModel.start_date);
       this.setLabel(promotionModel.label);
       this.setType(promotionModel.type);
       this.setTimes_used(promotionModel.times_used);
       this.setTitle(promotionModel.title);
       this.setUsage_per_customer(promotionModel.usage_per_customer);
       this.setUsage_count(promotionModel.usage_count);
       this.setUsage_limit(promotionModel.usage_limit);
        this.setUserInterested(false);
    }

    public boolean isUserInterested() {
        return isUserInterested;
    }

    public void setUserInterested(boolean userInterested) {
        isUserInterested = userInterested;
    }

    public double getActual_price() {
        return actual_price;
    }

    public String getCategory() {
        return category;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public double getDiscount_price() {
        return discount_price;
    }

    public Date getFinal_date() {
        return final_date;
    }

    public String getId() {
        return id;
    }

    public String getImage_url() {
        return image_url;
    }

    public String getLabel() {
        return label;
    }

    public Date getStart_date() {
        return start_date;
    }

    public double getTimes_used() {
        return times_used;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public double getUsage_count() {
        return usage_count;
    }

    public double getUsage_limit() {
        return usage_limit;
    }

    public double getUsage_per_customer() {
        return usage_per_customer;
    }

    public void setActual_price(double actual_price) {
        this.actual_price = actual_price;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDiscount_price(double discount_price) {
        this.discount_price = discount_price;
    }

    public void setFinal_date(Date final_date) {
        this.final_date = final_date;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public void setTimes_used(double times_used) {
        this.times_used = times_used;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setUsage_count(double usage_count) {
        this.usage_count = usage_count;
    }

    public void setUsage_limit(double usage_limit) {
        this.usage_limit = usage_limit;
    }

    public void setUsage_per_customer(double usage_per_customer) {
        this.usage_per_customer = usage_per_customer;
    }
}
