package com.theappchefs.ClinicaAvaria.Adapter;

/**
 * Created by Cyhon on 3/23/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.theappchefs.ClinicaAvaria.R;

import java.util.HashMap;
import java.util.List;


public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<String> mListHeaderData;
    private HashMap<String, List<String>> mListChildData;
    private static LayoutInflater inflater = null;

    public ExpandableListAdapter(Context context, List<String> listHeaderData, HashMap<String, List<String>> listChildData){
        this.mContext = context;
        this.mListChildData = listChildData;
        this.mListHeaderData = listHeaderData;
        this.inflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.mListChildData.get(this.mListHeaderData.get(groupPosition)).get(childPosition);
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder;
        holder = new ViewHolder();
        final String childText = (String)getChild(groupPosition, childPosition);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.list_item, null);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txt_subTitle = (TextView) convertView.findViewById(R.id.txtItem);
        holder.txt_subTitle.setText(childText);
        return convertView;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (this.mListChildData.get(this.mListHeaderData.get(groupPosition)) == null)
            return 0;
        else
            return this.mListChildData.get(this.mListHeaderData.get(groupPosition)).size();
    }

    @Override
    public int getGroupCount() {
        return this.mListHeaderData.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mListHeaderData.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolder holder;
        holder = new ViewHolder();
        View vi = convertView;
        String headerTitle = (String) getGroup(groupPosition);
        if (vi == null){
            vi = inflater.inflate(R.layout.list_group, null);
            vi.setTag(holder);
        }
        else{
            holder = (ViewHolder) vi.getTag();
        }

        holder.txt_header    = (TextView) vi.findViewById(R.id.txtgruop);
        holder.txt_header.setText(headerTitle);
        holder.indicator = (ImageView)vi.findViewById(R.id.indicator);
        if (groupPosition < 3)
        {
            holder.indicator.setVisibility(View.VISIBLE);
        }
        else if (groupPosition >2)
        {
            holder.indicator.setVisibility(View.INVISIBLE);
        }

        return vi;
    }


    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public static class ViewHolder{
        TextView txt_header;
        TextView txt_subTitle;
        ImageView indicator;
    }
}
