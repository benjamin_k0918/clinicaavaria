package com.theappchefs.ClinicaAvaria;

import com.theappchefs.ClinicaAvaria.model.PromotionData;
import com.theappchefs.ClinicaAvaria.model.RedeemedModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cyhon on 5/31/2018.
 */

public class Global {
    public static ArrayList<PromotionData> recentPromotion=  new ArrayList<PromotionData>();
    public static   ArrayList<PromotionData> interestedPromotion = new ArrayList<PromotionData>();
    public static PromotionData currentPromotionData = new PromotionData();

    public static RedeemedModel redeemedModel = new RedeemedModel();
    public static ArrayList<RedeemedModel> redeemedModelArrayList = new ArrayList<RedeemedModel>();
    public  Map<String,Object> redeemdPromotion = new HashMap<>();
    public static String login = "LOGIN";
    public static String loggedin = "loggedin";
    public static String loggedOut = "loggedOut";
    public static String user_id = "userId";
    public static String userEmail = "userEmail";
    public static String SNS = "sns";
    public static String isSns = "0";
    public static ArrayList<String> tags = new ArrayList<>();
    public static int position ;

    public static String currentUserEmail = "";

    public static String valueUserEmail = "";
    public static String valueUserId = "";

    static Global instance = null;
    public static Date preferredDate = null;


    public static Global getInstance(){
        if (instance == null){
            instance = new Global();
        }

        return instance;
    }


}
