package com.theappchefs.ClinicaAvaria.activities;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;
import com.theappchefs.ClinicaAvaria.Adapter.ExpandableListAdapter;
import com.theappchefs.ClinicaAvaria.Global;
import com.theappchefs.ClinicaAvaria.R;
import com.theappchefs.ClinicaAvaria.fragments.EditProfileFragment;
import com.theappchefs.ClinicaAvaria.fragments.MessageGateFragment;
import com.theappchefs.ClinicaAvaria.fragments.WebpageFragment;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import com.theappchefs.ClinicaAvaria.fragments.AvariaFragment;
import com.theappchefs.ClinicaAvaria.fragments.ContactFragment;
import com.theappchefs.ClinicaAvaria.fragments.PromotionFragment;
import com.theappchefs.ClinicaAvaria.fragments.ReservaFragment;
import com.jivosite.sdk.JivoDelegate;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements JivoDelegate{

    @BindView(R.id.toolbar)
    Toolbar custonActionBar;
    Boolean fromCardDetail = false;
    private TextView mTextMessage;
    private DrawerLayout mDrawwer;
    private ActionBarDrawerToggle actionbarToggle;
    static Fragment fragment;
    FirebaseAuth firebaseAuth;
    String[] group = {"Conocenos", "Medicina Estetica","Cirugía","Total Care","Videos","Contacto","Editar Perfil","Cerrar Sesión"};
    String[] group1 = {"Staff","Nuestra Clinica","Mision.Vision y valores","Trabaja Con Nosotros"};
    String [] group2 = {"POR PRODUCTO","  Toxia Botulinica","  Relleno con Acido Hialuronico","  Coctel de vitaminas inyectables","  Hilos de traccion y reparativos 100% reabsorbible",
            "POR ZONA","  Arrugas codigo de barras labio superior ","  Calidad de piel del rostro","  Cuello","  Frente.entrecjo y patas de gallo","  Labios","  Lineas de marioneta",
            "  Mejillas","  Menton","  Nariz","  Ojeras","  Pomulos","  Reborde mandibular","  Surco Nasogeniano","  Esteticos faciales","  Terapeuticos"};

    String [] group3 = {"Bichectomia","Implante de Mention","Lobuloplastia","Liposuccion de Papada","Blefaroplastia Superior","Blefaroplastia Inferior"};

    public static final String ACTION_SHOW_TEXT= "showText";

    List<String> listHeaderData;

    HashMap<String, List<String>> listChildData;

    FirebaseFirestore mFirebaseStore;
    FirebaseUser currentUser;

    ExpandableListAdapter expandableListAdapter;

    int flag = 1;
    int send;        int realwidth;
    @Nullable
    @Override
    public CharSequence onCreateDescription() {
        return super.onCreateDescription();
    }

    public  int fragmentTabIndex = 0;
    final int bottombarTextSize = 12;
    int real;
    String isSns = "";
    //controllers in toolbars
    BottomNavigationViewEx navigation;

    @BindView(R.id.toolbar_text)
    public  TextView toolbarTitle;

    @BindView(R.id.ic_search)
    public  ImageView toolbarImage;
    BroadcastReceiver broadcastReceiver;

    private void showAvariaFragments() {
        if (fragmentTabIndex != 1) {
            setSubFragment(new AvariaFragment());
        } else {
            return;
        }

    }

    public void showReservaFragment(ReservaFragment reservaFragment) {
        if (fragmentTabIndex != 2) {

            setSubFragment(new ReservaFragment());
        } else {
            return;
        }

    }

    private void showPromotionFragment() {
        if (fragmentTabIndex != 3) {
            PromotionFragment pantallaFragment = new PromotionFragment();
            FragmentTransaction fragmentTrans_promotion = getSupportFragmentManager().beginTransaction();
            fragmentTrans_promotion.replace(R.id.MainFrame, pantallaFragment, null);
            fragmentTrans_promotion.commit();
        } else {
            return;
        }
    }

    private void showContactFragment() {
        if (fragmentTabIndex != 4) {
            ContactFragment contactFragment = new ContactFragment();
            FragmentTransaction fragmentTrans_contact = getSupportFragmentManager().beginTransaction();
            fragmentTrans_contact.replace(R.id.MainFrame, contactFragment, null);
            fragmentTrans_contact.commit();
        } else
            return;
    }
    private void showMessageGateFragment() {
            MessageGateFragment contactFragment = new MessageGateFragment();
            FragmentTransaction fragmentTrans_contact = getSupportFragmentManager().beginTransaction();
            fragmentTrans_contact.replace(R.id.MainFrame, contactFragment, null);
            fragmentTrans_contact.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        listHeaderData = new ArrayList<String>();
        listChildData = new HashMap<String, List<String>>();

        final Intent getResults = getIntent();
        int target = getResults.getIntExtra("targetFrame",0);

        //make custom ActionBar
        custonActionBar.setVisibility(View.VISIBLE);

        //set custom_ActionBar as default Actionbar
        setSupportActionBar(custonActionBar);

        getSupportActionBar().show();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //when MainActivity. show Avaria
        showAvariaFragments();
        firebaseAuth = FirebaseAuth.getInstance();

        mFirebaseStore  = FirebaseFirestore.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        if (firebaseAuth.getCurrentUser() != null)
        {
         //   Toast.makeText(getApplicationContext(),Global.tags.get(0),Toast.LENGTH_SHORT).show();
        }

        SharedPreferences preferences = this.getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", android.content.Context.MODE_PRIVATE);
        isSns = preferences.getString(Global.SNS, Global.isSns);
        //push contents when Navigation Drawer appears
        final FrameLayout Main_Fragment = (FrameLayout) findViewById(R.id.MainFrame);
        mDrawwer = (DrawerLayout) findViewById(R.id.Left_Drawer);
        actionbarToggle = new ActionBarDrawerToggle(this, mDrawwer, R.string.open, R.string.close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }


            public void onDrawerOpened(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                Main_Fragment.setTranslationX(slideOffset * drawerView.getWidth());
                mDrawwer.bringChildToFront(drawerView);
                mDrawwer.requestLayout();
            }
        };
        mDrawwer.addDrawerListener(actionbarToggle);

        actionbarToggle.syncState();
        invalidateOptionsMenu();


        DocumentReference docRef = mFirebaseStore.collection("users").document(currentUser.getUid());
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {

                if (documentSnapshot.exists())
                {

                    Global.tags = (ArrayList<String>) documentSnapshot.get("tags");
                }
            }
        });


        //get Expandable ListView
        final ExpandableListView expandableListView = (ExpandableListView)findViewById(R.id.draweNavigation);

        //Load All data and display that DrawerNavigation
        prepareListData();
        expandableListAdapter = new ExpandableListAdapter(this, listHeaderData, listChildData);
        expandableListView.setAdapter(expandableListAdapter);

        //When gruop Item Clicked
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (groupPosition >2 && groupPosition<6)
                {
                    WebpageFragment webpageFragment = new WebpageFragment();

                    FragmentTransaction fragmentTrans_promotion = getSupportFragmentManager().beginTransaction();
                    Bundle sendData = new Bundle();
                    sendData.putInt("uri",groupPosition);
                    sendData.putInt("child",1000);
                    webpageFragment.setArguments(sendData);
                    fragmentTrans_promotion.replace(R.id.MainFrame, webpageFragment, null);
                    fragmentTrans_promotion.commit();
                    mDrawwer.closeDrawers();

                    navigation.setSelectedItemId(R.id.navigation_Avaria);
                }

                else if (groupPosition == 6) // Edit profile
                {
                    Intent intent = new Intent(MainActivity.this,EditProfileActivity.class);
                    startActivity(intent);
                    mDrawwer.closeDrawers();
                }
                else if (groupPosition == 7) // Logout
                {
              //      firebaseAuth.signOut();
                    SharedPreferences preferences = getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", android.content.Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor =      preferences.edit();
                    editor.putString(Global.login, Global.loggedOut);
                    if (isSns.equals("1"))
                    {
                        editor.putString(Global.SNS, "0");
                        LoginManager.getInstance().logOut();
                    }
                    editor.commit();

                    // Close the side menu
                    mDrawwer.closeDrawers();

                    // Go to login page
                    Intent intent = new Intent(MainActivity.this, SelectSignInMethodActivity.class);
                    startActivity(intent);
                    finish();
                }
                return false;
            }
        });

        //When Child Item Clicked
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

               WebpageFragment webpageFragment = new WebpageFragment();
                FragmentTransaction fragmentTrans_promotion = getSupportFragmentManager().beginTransaction();
                Bundle sendData = new Bundle();
                sendData.putInt("uri",groupPosition);
                sendData.putInt("child",childPosition);
                webpageFragment.setArguments(sendData);
                fragmentTrans_promotion.replace(R.id.MainFrame, webpageFragment, null);
                fragmentTrans_promotion.commit();
                mDrawwer.closeDrawers();
                navigation.setSelectedItemId(R.id.navigation_Avaria);
                return false;
            }
        });

        //load image on HomeButton and Enable homebutton

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.button_home);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Bottom Navigatin Bar initialization
        mTextMessage = (TextView) findViewById(R.id.message);
        navigation= (BottomNavigationViewEx) findViewById(R.id.bottom_navigation_bar);
        navigation.enableAnimation(false);
        navigation.enableShiftingMode(false);
        navigation.enableItemShiftingMode(false);
        navigation.setTextSize(bottombarTextSize);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("ReceivedFromsDialog"));

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        initOneSignalSDK();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    private void initOneSignalSDK(){
        OSPermissionSubscriptionState state =  OneSignal.getPermissionSubscriptionState();
        if (state.getSubscriptionStatus().getUserId() != null) {
            OneSignal.setSubscription(true);
        }
    }

    private void prepareListData(){

        //add header data
        for (int groupCount =0;groupCount<8;groupCount++)
        {
            listHeaderData.add(group[groupCount]);
        }
        //add subitems
        List<String> fist = new ArrayList<String >();

        fist.add(group1[0]);
        fist.add(group1[1]);
        fist.add(group1[2]);
        fist.add(group1[3]);

        List<String> second = new ArrayList<String >();
        for (int secondCount =0;secondCount<20;secondCount++)
        {
            second.add(group2[secondCount]);
        }

        List<String> third = new ArrayList<String >();
        for (int thirdCount = 0;thirdCount<6;thirdCount++)
        {
            third.add(group3[thirdCount]);
        }

        listChildData.put(listHeaderData.get(0), fist);
        listChildData.put(listHeaderData.get(1), second);
        listChildData.put(listHeaderData.get(2), third);

    }

    //set title on Actionbar
    public void setCustomActionBarTitle(String title){

        toolbarImage.setVisibility(View.GONE);

        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(title);

    }
    //set image as title on Actionbar
    private void setDefaultActionBar()
    {
        toolbarImage.setVisibility(View.VISIBLE);
        toolbarTitle.setVisibility(View.GONE);
    }

    @Override
    public void onEvent(String name, String data) {
            if (name.equals("url.click"))
           {
                 Intent i = new Intent(Intent.ACTION_VIEW);
                 i.setData(Uri.parse(data));
                startActivity(i);
             }

    }

    public void setSubFragment(Fragment fragment){
        FragmentTransaction     fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.MainFrame, fragment, null);
        fragmentTransaction.commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_Avaria:
                    setDefaultActionBar();
                    showAvariaFragments();
                    fragmentTabIndex = 1;
                    return true;

                case R.id.navigation_Reserva:
                    setCustomActionBarTitle(getString(R.string.reserva));
                    FragmentTransaction fra = getSupportFragmentManager().beginTransaction();
                    showReservaFragment(new ReservaFragment());
                    fragmentTabIndex = 2;
                    return true;

                case R.id.navigation_Promotion:
                    setCustomActionBarTitle(getString(R.string.promociones));
                    showPromotionFragment();
                    fragmentTabIndex = 3;
                    return true;

                case R.id.navigation_Contact:

                    if (fromCardDetail == false)
                    {
                        setCustomActionBarTitle(getString(R.string.contact));
                        showContactFragment();
                        fragmentTabIndex = 4;
                    }
                    else if(fromCardDetail == true)
                    {
                        setCustomActionBarTitle(getString(R.string.contact));
                        showMessageGateFragment();
                        fragmentTabIndex = 4;
                    }

                    return true;

            }
            return false;
        }
    };




    private NavigationView.OnNavigationItemSelectedListener mDrawerNavigationItemSelectistenter = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId())
            {
                case R.id.register:
                    fragmentTabIndex = 5;
                    return true;
                case R.id.login:
                    fragmentTabIndex = 5;
                    return true;
            }
            return false;
        }
    };

    private void closeDrawerAndSelectTab(){
        mDrawwer.closeDrawers();
        navigation.setSelectedItemId(R.id.avaria_View);
    }

    @Override
   public boolean onOptionsItemSelected(MenuItem item) {

        if (actionbarToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDrawwer != null) {
            // Pass any configuration change to the drawer toggles
            actionbarToggle.onConfigurationChanged(newConfig);
        }
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get the received random number
            String message = intent.getStringExtra("message");
            if (message == "Reserva"){
                showReservaFragment(new ReservaFragment());
                navigation.setSelectedItemId(R.id.navigation_Reserva);
            }
            else if (message == "Contact")
            {
                showMessageGateFragment();
                fromCardDetail = true;
                navigation.setSelectedItemId(R.id.navigation_Contact);
            }

        }
    };

}
