package com.theappchefs.ClinicaAvaria.activities;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.theappchefs.ClinicaAvaria.R;
import com.theappchefs.ClinicaAvaria.fragments.AvariaFragment;
import com.theappchefs.ClinicaAvaria.fragments.IngresaFragment;
import com.theappchefs.ClinicaAvaria.fragments.RegistarseFragment;
import com.theappchefs.ClinicaAvaria.fragments.SelectLoginMethodFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.btn_createAccount)
    Button btnCreateAccount ;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.splash)
    LinearLayout splashLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.btn_login)
    public void onBtnLoginClick(){
        setLoginButtonActive();
        splashLayout.setVisibility(View.GONE);

        IngresaFragment ingresaFragment = new IngresaFragment();
        FragmentTransaction fragmentTrans_Ingresa = getSupportFragmentManager().beginTransaction();
        fragmentTrans_Ingresa.replace(R.id.MainFrame,ingresaFragment,null);
        fragmentTrans_Ingresa.commit();

/*        Intent intent = new Intent(SplashActivity.this,SelectSignInMethodActivity.class);
        startActivity(intent);*/
    }


    @OnClick(R.id.btn_createAccount)
    public void onCreateAccountClick(){
        splashLayout.setVisibility(View.GONE);
        setCreateAccountButtonActive();

        RegistarseFragment registarseFragment = new RegistarseFragment();
        FragmentTransaction fragmentTrans_Ingresa = getSupportFragmentManager().beginTransaction();
        fragmentTrans_Ingresa.replace(R.id.MainFrame,registarseFragment,null);
        fragmentTrans_Ingresa.commit();

    }

    private  void setCreateAccountButtonActive()
    {
        btnCreateAccount.setBackground(getResources().getDrawable(R.drawable.ic_rounded_button));
        btnCreateAccount.setTextColor(getResources().getColor(R.color.colorWhite));

        btnLogin.setBackground(getResources().getDrawable(R.drawable.ic_rounded_buttontransparent));
        btnLogin.setTextColor(getResources().getColor(R.color.colorSelect));
    }
    private  void setLoginButtonActive()
    {
        btnLogin.setBackground(getResources().getDrawable(R.drawable.ic_rounded_button));
        btnLogin.setTextColor(getResources().getColor(R.color.colorWhite));
        btnCreateAccount.setBackground(getResources().getDrawable(R.drawable.ic_rounded_buttontransparent));
        btnCreateAccount.setTextColor(getResources().getColor(R.color.colorSelect));
    }

}
