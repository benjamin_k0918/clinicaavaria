package com.theappchefs.ClinicaAvaria.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Response;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.theappchefs.ClinicaAvaria.Global;
import com.theappchefs.ClinicaAvaria.MyPreferenceManager;
import com.theappchefs.ClinicaAvaria.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.theappchefs.ClinicaAvaria.SignalApplication;
import com.theappchefs.ClinicaAvaria.model.CAReopSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmConfiguration;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SelectSignInMethodActivity extends AppCompatActivity {
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 9001;

    private String TAG = "SELECTLOGINMETHODACTIVITY";
    private CallbackManager mCallbackManager;

    @BindView(R.id.button_facebook_login)
    LoginButton loginButton;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    String email = "", password = "", uId = "";
    String first_name = "";
    String last_name = "";
    String id = "";

    FirebaseFirestore db;
    private FirebaseAuth mAuth;

    String fbUserName = "";
    String fbUserEmail = "";
    String fbId = "";
    String fbAccessToken = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Add code to print out the key hash
        setContentView(R.layout.activity_select_sign_in_method);
        ButterKnife.bind(this);



        FacebookSdk.sdkInitialize(getApplicationContext());
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // Facebook Initialization
        initFacebookSDK();

        SharedPreferences preferences = this.getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", android.content.Context.MODE_PRIVATE);
        String loggedin = preferences.getString(Global.login, Global.loggedOut);
        if (loggedin.equals(Global.loggedin))
        {
            Global.valueUserEmail = preferences.getString(Global.userEmail,"email");
            Global.valueUserId = preferences.getString(Global.user_id,"user_id");
            Intent intent = new Intent(SelectSignInMethodActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }


    }

    private void initFacebookSDK() {
        mCallbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);


                fbAccessToken = loginResult.getAccessToken().getToken();
                Log.d(TAG, "Facebook : accessToken = " + fbAccessToken);

                // Show the loading progressBar
                progressBar.setVisibility(View.VISIBLE);

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        progressBar.setVisibility(View.GONE);
                        Log.d(TAG, "*** Facebook Login result = " + response.toString());
                        try {
                            if (object.has("id")) {
                                fbId = object.getString("id");
                            }

                            if (object.has("first_name")) {
                                first_name = object.getString("first_name");

                            }
                            if (object.has("last_name")) {
                                last_name = object.getString("last_name");
                            }
                            // get user name
                            if (object.has("name")) {
                                fbUserName = object.getString("name");
                            }

                            // get user email
                            if (object.has("email")) {
                                fbUserEmail = object.getString("email");
                            }

                            //save fb user credentials into Firebase DB
                            String arrayString[] = fbUserName.split("\\s+");

                            // Save first name
                            if (arrayString.length > 0) {
                                if (!arrayString[0].isEmpty()) {
                                    first_name = arrayString[0];
                                }
                            }

                            // Save last name
                            if (arrayString.length > 1) {
                                if (!arrayString[1].isEmpty()) {
                                    last_name = arrayString[1];
                                }
                            }

                            // Firebase Login using Fb accessToken
                            handleFacebookAccessToken(fbAccessToken);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
    }


    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.theappchefs.ClinicaAvaria", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }

    @OnClick(R.id.layout_facebook_login)
    public void onFacebookLogin() {
        loginButton.performClick();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

    }

    @OnClick(R.id.btn_login_email)
    public void onLoginWithEmail() {
        Intent intent = new Intent(SelectSignInMethodActivity.this, SplashActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_google_login)
    public void onGoogleLogin() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                email = account.getEmail();
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                Toast.makeText(getApplicationContext(), "Google SignIn failed.", Toast.LENGTH_LONG).show();
                // ...
            }
        }
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    // authenticate with Firebase after FB login
    private void handleFacebookAccessToken(String token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        progressBar.setVisibility(View.VISIBLE);

        AuthCredential credential = FacebookAuthProvider.getCredential(token);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            saveFbUserInDB(user);

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(SelectSignInMethodActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(SelectSignInMethodActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                });
    }

    // Save the user logged in using Facebook into remote DB
    private void saveFbUserInDB(FirebaseUser fbUser) {
        final Map<String, Object> userMap = new HashMap<>();
        userMap.put("email", fbUserEmail);
        userMap.put("first_name", first_name);
        userMap.put("last_name", last_name);
        userMap.put("preferred_customer", false);

        String uId = fbUser.getUid();
        userMap.put("id", uId);

        db.collection("users").document(uId).set(userMap).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "User successfully added!");
                gotoHomeScreen();
            }
        })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "Error adding user : " + e);
                Toast.makeText(SelectSignInMethodActivity.this, e.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Go to Main page
    private void gotoHomeScreen() {
        Realm mRealm = Realm.getDefaultInstance();
        mRealm.beginTransaction();
        CAReopSession.getInstance().setUserId(mAuth.getCurrentUser().getUid());
        CAReopSession.getInstance().setToken(mAuth.getCurrentUser().getIdToken(true).toString());
        mRealm.commitTransaction();
        SharedPreferences preferences = this.getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =      preferences.edit();
        editor.putString(Global.login, Global.loggedin);

        editor.commit();
        Intent intent = new Intent(SelectSignInMethodActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        progressBar.setVisibility(View.VISIBLE);

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);

                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            final Map<String, Object> userMap = new HashMap<>();
                            userMap.put("email", email);
                            userMap.put("first_name", acct.getGivenName());
                            userMap.put("last_name", acct.getFamilyName());
                            userMap.put("preferred_customer", false);
                            userMap.put("id", user.getUid());
                            DocumentReference ref = db.collection("user").document();
                            db.collection("users").document(user.getUid()).set(userMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    SharedPreferences preferences = getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", android.content.Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor =      preferences.edit();
                                    editor.putString(Global.login, Global.loggedin);
                                    editor.putString(Global.SNS,"1");
                                    editor.commit();
                                    gotoHomeScreen();
                                }
                            });

                        }else{
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(SelectSignInMethodActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
