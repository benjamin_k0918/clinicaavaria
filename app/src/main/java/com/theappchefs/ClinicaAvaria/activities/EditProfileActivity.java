package com.theappchefs.ClinicaAvaria.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.theappchefs.ClinicaAvaria.Global;
import com.theappchefs.ClinicaAvaria.R;
import com.theappchefs.ClinicaAvaria.fragments.ChangePasswordFragment;
import com.theappchefs.ClinicaAvaria.fragments.PromotionFragment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditProfileActivity extends AppCompatActivity {

    @BindView(R.id.edittext_firstName)
    EditText editTextFirstName;

    @BindView(R.id.editText_lastName)
    EditText editTextlastName;

    @BindView(R.id.editTexT_rut)
    EditText editTextRut;

    @BindView(R.id.editText_phone)
    EditText editTextPhone;

    String firstName = "";
    String lastName = "";
    String rut = "";
    String phone = "";

    @BindView(R.id.tv_changePasswords)
    TextView changePassword;

    @BindView(R.id.btn_save)
    Button btn_save;

    FirebaseAuth firebaseAuth;
    FirebaseFirestore db;
    FirebaseUser currentUser;

    @BindView(R.id.MainFrame)
    FrameLayout mainFrame;


    @BindView(R.id.MainLayout)
    LinearLayout  mainLayout;

    @BindView(R.id.toolbar)
    Toolbar custonActionBar;

    String title = "Editar Perfil";

    @BindView(R.id.toolbar_title)
    TextView tvTitle;//
    int currentFragment = 1;
    ProgressDialog progressDialog ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        custonActionBar.setVisibility(View.VISIBLE);

        //set custom_ActionBar as default Actionbar
        setSupportActionBar(custonActionBar);
        progressDialog = new ProgressDialog(this);
        getSupportActionBar().show();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.cancelbutton);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        db = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();


        db.collection("users").document(currentUser.getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                progressDialog.show();

                if (documentSnapshot.get("first_name") != null)
                {
                    firstName = documentSnapshot.get("first_name").toString();
                }if (documentSnapshot.get("last_name") != null){
                    lastName = documentSnapshot.get("last_name").toString();
                }if (documentSnapshot.get("rut") != null)
                {
                    rut = documentSnapshot.get("rut").toString();
                }if (documentSnapshot.get("phone_number") != null)
                {
                    phone = documentSnapshot.get("phone_number").toString();
                }

                initUI();
            }
        });
    }

    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private boolean isRutValidate(){
        rut = editTextRut.getText().toString().trim();
        String tempRut = rut;
        boolean validacion = false;
        try {
            tempRut =  tempRut.toUpperCase();
            tempRut = tempRut.replace(".", "");
            tempRut = tempRut.replace("-", "");
            int rutAux = Integer.parseInt(tempRut.substring(0, tempRut.length() - 1));

            char dv = tempRut.charAt(tempRut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }

    private void initUI(){
        progressDialog.dismiss();
        editTextFirstName.setText(firstName);
        editTextlastName.setText(lastName);
        editTextRut.setText(rut);
        editTextPhone.setText(phone);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
                processBack();

        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {

        processBack();

    }
    private void processBack(){
        if (currentFragment == 2)
        {
            mainLayout.setVisibility(View.VISIBLE);
            mainFrame.setVisibility(View.GONE);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.cancelbutton);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            tvTitle.setText(title);

            currentFragment = 1;
        }
        else if (currentFragment == 1)
        {
            finish();
        }
    }

    @OnClick(R.id.btn_save)
    public void onSaveClicked() {
        firstName = editTextFirstName.getText().toString().trim();
        lastName = editTextlastName.getText().toString().trim();
        rut = editTextRut.getText().toString().trim();
        phone = editTextPhone.getText().toString().trim();

        if (firstName.length() == 0 || lastName.length() == 0 || rut.length() == 0) {
            Toast.makeText(this, "All fields are required.", Toast.LENGTH_SHORT).show();
        } else {
            if (isRutValidate()){
                String userId = "";
                if (currentUser != null) {
                    userId = currentUser.getUid();
                } else {
                    userId = Global.valueUserId;
                }
                db.collection("users").document(userId).update("first_name", firstName);
                db.collection("users").document(userId).update("last_name", lastName);
                db.collection("users").document(userId).update("rut", rut);
                db.collection("users").document(userId).update("phone_number", phone);
                Toast.makeText(getApplicationContext(),"Usario actualizado.",Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(this,"Please input valid Rut",Toast.LENGTH_SHORT).show();
            }

        }
    }//Cambiar contrasena

    @OnClick(R.id.tv_changePasswords)
    public void onChangePasswordClicked(){
        mainLayout.setVisibility(View.GONE);
        mainFrame.setVisibility(View.VISIBLE);
        ChangePasswordFragment promotionFragment  = new ChangePasswordFragment();
        FragmentManager fragmentManager= getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.MainFrame,promotionFragment,null);
        fragmentTransaction.commit();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.backbutton);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        currentFragment = 2;

           tvTitle.setText("Cambiar contraseña");
    }


}
