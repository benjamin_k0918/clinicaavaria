package com.theappchefs.ClinicaAvaria.Listener;

/**
 * Created by Cyhon on 3/4/2018.
 */

public interface CustomClickListener {

    void onItemClick(int position);
}
