package com.theappchefs.ClinicaAvaria.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.theappchefs.ClinicaAvaria.Global;
import com.theappchefs.ClinicaAvaria.R;
import com.makeramen.roundedimageview.RoundedImageView;

import com.theappchefs.ClinicaAvaria.activities.MainActivity;
import com.theappchefs.ClinicaAvaria.model.PromotionData;
import com.theappchefs.ClinicaAvaria.model.PromotionModel;
import com.theappchefs.ClinicaAvaria.model.RedeemedModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.BindView;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class CardDetailFragment extends Fragment {

    @BindView(R.id.memberImage)
    RoundedImageView photo;

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.txt_price)
    TextView actualprice;

    @BindView(R.id.txt_percentage_discount)
    TextView discountpercent;

    @BindView(R.id.txt_date)
    TextView recoverydate;

    @BindView(R.id.suggest)
    TextView suggestcost;

    @BindView(R.id.expire_date)
    LinearLayout expiredate;

    @BindView(R.id.btn_interested)
    Button btninterest;

    @BindView(R.id.btn_exchange)
    Button btn_exchange;

    @BindView(R.id.backbutton)
    ImageView backbutton;

    Realm mRealm;

    String photopath;
    String strdescription;
    String head = "";
    int count = 0;
    String userId = "";
    ArrayList<String> tempRedeem = new ArrayList<String>();

    FirebaseFirestore db;
    FirebaseUser currentUser;
    FirebaseAuth firebaseAuth;
    Global global;
    PromotionData promotionData;
    int number;
    private RequestOptions requestOptions = new RequestOptions();

    String id = "", code = "",title = "",strrecoverydate = "",stractualprice = "";Boolean isUserInterested = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View containerView=inflater.inflate(R.layout.fragment_card_detail, container, false);
        ButterKnife.bind(this,containerView);

        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        //Receive parameters from promotion fragment
        db = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();


        global = Global.getInstance();
        Realm.init(getActivity());  // add this line
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);

        mRealm = Realm.getDefaultInstance();

        Bundle bundle = getArguments();
        id = bundle.getString("id");
        photopath = bundle.getString("photopath");
        strdescription = bundle.getString("description");
        stractualprice = String.valueOf(bundle.getDouble("actual_price")).replace(".0","");
        title = String.valueOf(bundle.getDouble("percentage_discount")).replace(".0","");
        strrecoverydate = bundle.getString("recovery_date");
        isUserInterested = bundle.getBoolean("isInterested");
        head = bundle.getString("head");
        code = bundle.getString("code");
        //set variable to controllers
    //    Picasso.with(getActivity()).load(photopath).placeholder(getActivity().getResources().getDrawable(R.drawable.ic_default)).into(photo);
        Glide.with(getActivity()).setDefaultRequestOptions(requestOptions).load(photopath).into(photo);


        actualprice.setText(stractualprice + " CLP");

        suggestcost.setText(title+ " CLP");
        recoverydate.setText(strrecoverydate);
        description.setText(strdescription);
        discountpercent.setText(head);


        suggestcost.setPaintFlags(suggestcost.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);

        if (isUserInterested == true)
        {
           btninterest.setVisibility(View.GONE);
           btn_exchange.setVisibility(View.VISIBLE);

        }else if (isUserInterested == false)
        {
            btn_exchange.setVisibility(View.GONE);
            btninterest.setVisibility(View.VISIBLE);
        }
        return containerView;
    }

    @OnClick(R.id.backbutton)
    public  void onBackButtonClicked(){
        gotoBackPage();
    }

    private void gotoBackPage(){
        PromotionFragment promotionFragment  = new PromotionFragment();
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.MainFrame,promotionFragment,null);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.btn_exchange)
    public void onExchangeClicked(){

        showPromotionCodeDailog();
        btn_exchange.setBackground(getResources().getDrawable(R.drawable.ic_rounded_button));
        btn_exchange.setTextColor(getResources().getColor(R.color.colorWhite));
    }

    @OnClick(R.id.btn_interested)
    public void onInterestClicked(){

        if (!global.recentPromotion.isEmpty())
        {
            mRealm.beginTransaction();
            promotionData = mRealm.createObject(PromotionData.class);
            promotionData.setId(global.recentPromotion.get(Global.position).getId());
            promotionData.setFinal_date(global.recentPromotion.get(Global.position).getFinal_date());
            promotionData.setStart_date(global.recentPromotion.get(Global.position).getStart_date());
            promotionData.setActual_price(global.recentPromotion.get(Global.position).getActual_price());
            promotionData.setDiscount_price(global.recentPromotion.get(Global.position).getDiscount_price());
            promotionData.setLabel(global.recentPromotion.get(Global.position).getLabel());
            promotionData.setCategory(global.recentPromotion.get(Global.position).getCategory());
            promotionData.setTitle(global.recentPromotion.get(Global.position).getTitle());
            promotionData.setImage_url(global.recentPromotion.get(Global.position).getImage_url());
            promotionData.setCode(global.recentPromotion.get(Global.position).getCode());
            promotionData.setDescription(global.recentPromotion.get(Global.position).getDescription());
            promotionData.setUserInterested(true);
            promotionData.setTimes_used(global.recentPromotion.get(Global.position).getTimes_used());
            promotionData.setType(global.recentPromotion.get(Global.position).getType());
            promotionData.setUsage_count(global.recentPromotion.get(Global.position).getUsage_count());
            promotionData.setUsage_limit(global.recentPromotion.get(Global.position).getUsage_limit());
            mRealm.commitTransaction();
        }

        btninterest.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_rounded_button));
        btninterest.setTextColor(getResources().getColor(R.color.colorWhite));
        btninterest.setText(R.string.see_promotion);
        expiredate.setVisibility(View.VISIBLE);
        count++;
        showDialog();
    }

    // show the redeem promotion dialog
    protected void showPromotionCodeDailog(){

        final Dialog dialog = new Dialog(getActivity());
        final View view  = getActivity().getLayoutInflater().inflate(R.layout.promotion_code_dialog, null);

        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.setCancelable(false);

        if (currentUser == null)
        {
            userId = Global.valueUserId;
        }else {
            userId = currentUser.getUid();
        }

        Button btn_ok = view.findViewById(R.id.ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText editTextPromotionCode = view.findViewById(R.id.promotion_code);
                String strPromotionCode = editTextPromotionCode.getText().toString().trim();
                if (strPromotionCode.equals(code)){

                    db.collection("users").document(userId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {

                        }
                    });
                    List<String> namwe = new ArrayList<>();
                    global.redeemdPromotion.put("id",id);
                    global.redeemdPromotion.put("promotion_id",id);
                    global.redeemdPromotion.put("user_id",userId);
                    global.redeemdPromotion.put("date",new Date());
                    Gson gson = new Gson(); // com.google.gson.Gson


                    db.collection("users").document(userId).collection("redeemed_promotions").add(global.redeemdPromotion).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            mRealm.beginTransaction();
                            global.redeemedModel = mRealm.createObject(RedeemedModel.class);
                            global.redeemedModel.setId(id);
                            global.redeemedModel.setPromotionId(id);
                            global.redeemedModel.setPromotionId(userId);
                            global.redeemedModel.setDate(new Date());

                            PromotionData promotionData = mRealm.where(PromotionData.class).equalTo("id",Global.currentPromotionData.getId()).findFirst();
                            promotionData.deleteFromRealm();
                            mRealm.commitTransaction();
                            Global.interestedPromotion.remove(Global.currentPromotionData);
                            dialog.dismiss();

                            // Go to promotion list page
                            gotoBackPage();
                        }
                    });

                }else{
                    Toast.makeText(getActivity(),"You have entered wrong code",Toast.LENGTH_SHORT).show();
                }

            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                btn_exchange.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_rounded_buttonwhite));
                btn_exchange.setTextColor(getResources().getColor(R.color.colorSelect));
            }
        });

        dialog.show();
    }

    protected void showDialog(){

        final Dialog dialog = new Dialog(getActivity());
        final View view  = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog, null);

        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

                btninterest.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_rounded_buttonwhite));
                btninterest.setTextColor(getResources().getColor(R.color.colorSelect));
                btninterest.setText(getString(R.string.want_to_book));
                expiredate.setVisibility(View.GONE);
            }
        });
        //controllers
        RoundedImageView photo = view.findViewById(R.id.memberImaegdialog);
        TextView txtTitle = view.findViewById(R.id.txt_title);

        TextView txtPrice = view.findViewById(R.id.price);
        TextView txtDate = view.findViewById(R.id.date);
        TextView txtSuggestPrice = view.findViewById(R.id.suggest_price);


        txtSuggestPrice.setPaintFlags(txtSuggestPrice.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
        txtSuggestPrice.setText(title + " CLP");
        txtTitle.setText(head);
   //     Picasso.with(getActivity()).load(photopath).placeholder(getActivity().getResources().getDrawable(R.drawable.ic_default)).into(photo);
        Glide.with(getActivity()).setDefaultRequestOptions(requestOptions).load(photopath).into(photo);

        txtPrice.setText(stractualprice + " CLP");
        txtDate.setText(strrecoverydate);

        Button onlineRegister = view.findViewById(R.id.onlineRegister);
        Button phoneRegister = view.findViewById(R.id.phoneRegister);
        Button sendMessage = view.findViewById(R.id.sendMessage);

        phoneRegister.setEnabled(true);
        sendMessage.setEnabled(true);
        onlineRegister.setEnabled(true);
        phoneRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btninterest.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_rounded_buttonwhite));
                btninterest.setTextColor(getResources().getColor(R.color.colorSelect));
                btninterest.setText(getString(R.string.want_to_book));
                expiredate.setVisibility(View.GONE);
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + "+56961426693"));
                getActivity().startActivity(intent);
            }
        });

        onlineRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent("ReceivedFromsDialog");
                // You can also include some extra data.
                intent.putExtra("message", "Reserva");
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
              //  getActivity().finish();
                dialog.dismiss();
            }
        });

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("ReceivedFromsDialog");
                // You can also include some extra data.
                intent.putExtra("message", "Contact");
                LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                dialog.dismiss();
            }
        });
        dialog.show();
    };

    @Override
    public void onDestroy() {
        mRealm.close();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
        super.onDestroy();
    }


    // TODO: Rename method, update argument and hook method into UI event

}
