package com.theappchefs.ClinicaAvaria.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.theappchefs.ClinicaAvaria.R;

import com.theappchefs.ClinicaAvaria.activities.MainActivity;


public class ReservaFragment extends Fragment {

    final  String URL_RESERVA_SITE = "http://agendamiento.softwaredentalink.com/agendas/online/115a406236790d7d59d88fcb320e44d0fa97d026";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final  View containerView = inflater.inflate(R.layout.fragment_reserva, container, false);



        class ProgressTask extends AsyncTask<String, Void, String> {
            WebView mWebView;
            @Override
            protected String doInBackground(String... paraks) {


                return new String();
            }

            @Override
            protected void onPreExecute() {
                // TODO Auto-generated method stub

                mWebView = (WebView) containerView.findViewById(R.id.agendamiento_view);
                mWebView.loadUrl(URL_RESERVA_SITE);

                // Enable Javascript
                WebSettings webSettings = mWebView.getSettings();
                webSettings.setJavaScriptEnabled(true);

                // Force links and redirects to open in the WebView instead of in a browser
                mWebView.setWebViewClient(new WebViewClient());
                super.onPreExecute();

            }

            @Override
            protected void onPostExecute(String result) {
                // TODO Auto-generated method stub
                super.onPostExecute(result);

                mWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onLoadResource(WebView view, String url) {
                        try {
                            mWebView.loadUrl("javascript:(window.onload = function() { " +
                                    "(elem1 = document.getElementById('id1')); elem.parentNode.removeChild(elem1); " +
                                    "(elem2 = document.getElementById('head')); elem2.parentNode.removeChild(elem2); " +
                                    "})()");
                            mWebView.loadUrl("javascript:document.getElementsByClassName('navbar navbar-inverse navbar-fixed-top')[0].style.display='none'");

                            mWebView.evaluateJavascript("document.body.style.backgroundColor = \"white\";\n" +
                                    "                    var header = document.getElementById(\"topheader\");\n" +
                                    "                    header.remove();\n var main = document.getElementsByClassName(\"main\")[0]; main.style.boxShadow = \"none\";" +
                                    "var footer = document.getElementsByClassName(\"footer-container\")[0]; var foreigner = document.createElement('foreigner');" +
                                    "foreigner.innerHTML = \"<br><br>Ingrese 11111111-1 si usted no tiene un RUT\";var loginRUT = document.getElementById(\"login-rut\");" +
                                    "loginRUT.appendChild(foreigner);footer.remove();main.reload();document.querySelector('.footer').remove();",null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
        // Force links and redirecs to open in the WebView instead of in a browser
        new ProgressTask().execute();

        return containerView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true); // Do not forget this!!!
    }

    public interface OnFragmentInteractionListener {
    }
}
