package com.theappchefs.ClinicaAvaria.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.theappchefs.ClinicaAvaria.R;


public class WebpageFragment extends Fragment {
    final  String[] groupSiteURL = {"https://clinicaavaria.cl/conocenos/clinica/","https://clinicaavaria.cl/categoria/zona/g","https://clinicaavaria.cl/categoria/cirugia/","https://clinicaavaria.cl/clinica-avaria-total-care/"," https://clinicaavaria.cl/videos","https://clinicaavaria.cl/contacto/"};
    final String[][] normalURL = {{"https://clinicaavaria.cl/conocenos/staff","https://clinicaavaria.cl/conocenos/clinica/","https://clinicaavaria.cl/conocenos/mission-y-vision/","https://clinicaavaria.cl/conocenos/trabaja-con-nosotros/"}
    ,{"https://clinicaavaria.cl/categoria/producto/","https://clinicaavaria.cl/categoria/producto/toxina-botulinica/","https://clinicaavaria.cl/categoria/producto/relleno-con-acido-hialuronico/","https://clinicaavaria.cl/categoria/producto/coctel-de-vitaminas-inyectables/"
            ,"https://clinicaavaria.cl/categoria/producto/hilos-de-traccion-y-reparativos-100-reabsorbible/","https://clinicaavaria.cl/categoria/zona/","https://clinicaavaria.cl/categoria/zona/arrugas-codigo-de-barras-labio-superior/"
            ,"https://clinicaavaria.cl/categoria/zona/calidad-de-piel-del-rostro/","https://clinicaavaria.cl/categoria/zona/cuello/"," https://clinicaavaria.cl/categoria/zona/frente-entrecejo-y-patas-de-gallo/",
            "https://clinicaavaria.cl/categoria/zona/labios/", "https://clinicaavaria.cl/categoria/zona/lineas-de-marioneta/","https://clinicaavaria.cl/categoria/zona/mejillas/",
            "https://clinicaavaria.cl/categoria/zona/menton/","https://clinicaavaria.cl/categoria/zona/nariz/","https://clinicaavaria.cl/categoria/zona/ojeras/"
            ,"https://clinicaavaria.cl/categoria/zona/pomulos/","https://clinicaavaria.cl/categoria/zona/reborde-mandibular/","https://clinicaavaria.cl/categoria/zona/surco-nasogeniano/",
            "https://clinicaavaria.cl/categoria/tratamiento/","https://clinicaavaria.cl/categoria/tratamiento/esteticos-faciales/","https://clinicaavaria.cl/categoria/tratamiento/terapeuticos/"}
            ,{"https://clinicaavaria.cl/tratamiento/bichectomia/","https://clinicaavaria.cl/tratamiento/implante-de-menton/","https://clinicaavaria.cl/tratamiento/lobuloplastia-2/",
            "https://clinicaavaria.cl/tratamiento/liposuccion-de-papada/","https://clinicaavaria.cl/tratamiento/blefaroplastia-superior/","https://clinicaavaria.cl/tratamiento/blefaroplastia-inferior/"}};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View containerView = inflater.inflate(R.layout.fragment_webpage, container, false);




        class ProgressTask1 extends AsyncTask<Void, Void, Void> {

            Bundle bundle;WebView mWebView;
            int uri;int childURI;
            @Override
            protected Void doInBackground(Void... voids) {


                return null;
            }


            @Override
            protected void onPreExecute() {
                // TODO Auto-generated method stub
                super.onPreExecute();
                bundle = getArguments();
                uri= bundle.getInt("uri");
                childURI = bundle.getInt("child");
                mWebView= (WebView) containerView.findViewById(R.id.agendamiento_view);
                WebSettings webSettings = mWebView.getSettings();
                webSettings.setJavaScriptEnabled(true);
            }


            @Override
            protected void onPostExecute(Void result) {
                // TODO Auto-generated method stub
                super.onPostExecute(result);

                if (childURI == 1000)
                {
                    mWebView.loadUrl(groupSiteURL[uri]);
                }
                else
                {
                    mWebView.loadUrl(normalURL[uri][childURI]);
                }

                // Force links and redirects to open in the WebView instead of in a browser
                mWebView.setWebViewClient(new WebViewClient() {
                    @Override
                    public void onLoadResource(WebView view, String url) {


                        try {
                            mWebView.loadUrl("javascript:(window.onload = function() { " +
                                    "(elem1 = document.getElementById('id1')); elem.parentNode.removeChild(elem1); " +
                                    "(elem2 = document.getElementById('head')); elem2.parentNode.removeChild(elem2); " +
                                    "})()");
                            mWebView.loadUrl("javascript:document.getElementsByClassName('navbar navbar-inverse navbar-fixed-top')[0].style.display='none'");
                            mWebView.evaluateJavascript("document.querySelector('.header').remove();",null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        new ProgressTask1().execute();
        return containerView;
    }


}
