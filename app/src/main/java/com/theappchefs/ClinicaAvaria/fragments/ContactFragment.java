package com.theappchefs.ClinicaAvaria.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import java.util.Locale;


import com.theappchefs.ClinicaAvaria.R;
import com.jivosite.sdk.*;


public class ContactFragment extends Fragment{

    JivoSdk jivoSdk;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment

        View containerView=inflater.inflate(R.layout.fragment_contact, container, false);
        String lang = Locale.getDefault().getLanguage().indexOf("en")>=0 ? "en": "ru";
        jivoSdk = new JivoSdk((WebView) containerView.findViewById(R.id.webview),lang);
        jivoSdk.delegate = (JivoDelegate) getActivity();
        jivoSdk.prepare();
        return containerView;
    }

}
