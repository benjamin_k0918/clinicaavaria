package com.theappchefs.ClinicaAvaria.fragments;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.theappchefs.ClinicaAvaria.Global;
import com.theappchefs.ClinicaAvaria.R;

import butterknife.ButterKnife;


public class ForgotPasswordFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    FirebaseAuth firebaseAuth;
    ProgressDialog dialog;
    LinearLayout text_warning;
    LinearLayout buffer;
    EditText email;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment

        View containerView=inflater.inflate(R.layout.fragment_forgot_password, container, false);
        ButterKnife.bind(this,containerView);
        Button btnRecoverPassword = (Button)containerView.findViewById(R.id.recover_password);
        text_warning = (LinearLayout) containerView.findViewById(R.id.text_warning);
        buffer = (LinearLayout)containerView.findViewById(R.id.BUFFER);
        email = (EditText)containerView.findViewById(R.id.text_email);
        ActionBar actionBar =getActivity().getActionBar();
        firebaseAuth = FirebaseAuth.getInstance();
        dialog = new ProgressDialog(getActivity());

        TextView textView = containerView.findViewById(R.id.cancel);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IngresaFragment ingresaFragment  = new IngresaFragment();
                FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.MainFrame,ingresaFragment,null);
                fragmentTransaction.commit();
            }
        });

        btnRecoverPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.setMessage("Waiting for server reply");
                dialog.show();
                final String str_email = email.getText().toString().trim();
                final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (str_email.matches(emailPattern) && str_email.length() >0)
                {
                    try{
                        firebaseAuth.sendPasswordResetEmail(str_email).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Global.currentUserEmail = str_email;
                                Toast.makeText(getActivity(),"Request has sent to your email" + str_email,Toast.LENGTH_LONG).show();
                                ForgotPasswordSuccessFragment forgotPasswordSuccessFragment  = new ForgotPasswordSuccessFragment();
                                FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.MainFrame,forgotPasswordSuccessFragment,null);

                                fragmentTransaction.commit(); }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(getActivity(),"Network status is bad or you need to install Google Play Service",Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                else
                {
                    dialog.dismiss();
                    buffer.setVisibility(View.GONE);
                    text_warning.setVisibility(View.VISIBLE);
                }
            }
        });
        return containerView;
    }

    @Override
    public void onDestroy() {
        dialog.cancel();
        super.onDestroy();
    }
}
