package com.theappchefs.ClinicaAvaria.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.theappchefs.ClinicaAvaria.Global;
import com.theappchefs.ClinicaAvaria.MyPreferenceManager;
import com.theappchefs.ClinicaAvaria.R;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ChangePasswordFragment extends Fragment {
    FirebaseAuth firebaseAuth;
    FirebaseFirestore db;
    FirebaseUser currentUser;

    @BindView(R.id.editTextFormer)
    EditText formerPassword;

    @BindView(R.id.editText_new)
    EditText newPassword;

    @BindView(R.id.editText_new_confirm)
            EditText confirmPassword;
    String isSns;

    String TAG = "ChangePasswordFragment";

    @BindView(R.id.tv_error_msg)
    TextView tvError;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_change_password, container, false);
        ButterKnife.bind(this,containerView);


        db = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();


        SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", Context.MODE_PRIVATE);
         isSns = preferences.getString(Global.SNS, Global.isSns);
        return containerView;
    }


    @OnClick(R.id.btn_change)
    public void onChangeClicked() {
        String confirmedPass = confirmPassword.getText().toString().trim();

        String pass = formerPassword.getText().toString().trim();

        final String newPass = newPassword.getText().toString().trim();
    //    user = FirebaseAuth.getInstance().getCurrentUser();
        String userEmail = "";

        if (currentUser == null)
        {
            userEmail = Global.valueUserEmail;
        }else {
            userEmail =   currentUser.getEmail();
        }
// Get auth credentials from the user for re-authentication. The example below shows
// email and password credentials but there are multiple possible providers,
// such as GoogleAuthProvider or FacebookAuthProvider.

      if (pass.length() ==0)
        {
            Toast.makeText(getActivity(),  "Please enter your former password.",Toast.LENGTH_SHORT).show();

        }
        else if (!confirmedPass.equals(newPass))
      {
          Toast.makeText(getActivity(),  "The new password does not match. Please double check the fields entered.",Toast.LENGTH_SHORT).show();

      }
        else if (newPass.length() == 0)
        {
            Toast.makeText(getActivity(),  "Please enter your new password.",Toast.LENGTH_SHORT).show();
        }
        else if (pass.length()!= 0)
        {
            AuthCredential credential = EmailAuthProvider
                    .getCredential(userEmail, pass);

// Prompt the user to re-provide their sign-in credentials

        currentUser.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            currentUser.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "Password updated");
                                        Toast.makeText(getActivity(),"SUCCESS",Toast.LENGTH_SHORT).show();
                                        tvError.setVisibility(View.GONE);
                                    } else {
                                        Log.d(TAG, "Failed to update the password");
                                        Toast.makeText(getActivity(),"FAIL",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            tvError.setText(task.getException().getMessage());
                            tvError.setVisibility(View.VISIBLE);
                        } }
                });
        }


    }


    @OnClick(R.id.MainLayout)
    public void onMainLayoutClicked(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
