package com.theappchefs.ClinicaAvaria.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.storage.StorageReference;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.StorageReference;

import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.onesignal.OneSignal;
import com.squareup.picasso.Picasso;
import com.theappchefs.ClinicaAvaria.Global;
import com.theappchefs.ClinicaAvaria.activities.MainActivity;
import com.theappchefs.ClinicaAvaria.model.CardViewModel;

import com.theappchefs.ClinicaAvaria.R;
import com.makeramen.roundedimageview.RoundedImageView;
import com.theappchefs.ClinicaAvaria.model.PromotionData;
import com.theappchefs.ClinicaAvaria.model.PromotionModel;
import com.theappchefs.ClinicaAvaria.model.RedeemedModel;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;
import javax.microedition.khronos.opengles.GL;

import static com.theappchefs.ClinicaAvaria.Global.redeemedModel;


public class PromotionFragment extends Fragment {
    SimpleAdapter adapter;
    private List<CardViewModel> CardInfo = new ArrayList<>();
    Button interest,recent;
    Global global;
    FirebaseAuth firebaseAuth;
    ProgressDialog dialog;

    @BindView(R.id.prefereed_layout)
    LinearLayout  preferredLayout;

    @BindView(R.id.normal_layout)
            LinearLayout normalLaout;

    RedeemedModel redeemedModel  = null;
    RealmResults<PromotionData> results;
    String user_id;Realm mRealm;
    public static Fragment newInstance() {
        return new PromotionFragment();
    }
    FirebaseFirestore mFirebaseStore;
    ArrayList<PromotionData> promotion_array = new ArrayList<PromotionData>();

    @BindView(R.id.tv_benefit) TextView tvBenefit;

    @BindView(R.id.tv_percent_discount) TextView tvDiscount;

     FirebaseUser currentUser;
    RecyclerView recyclerView;
    ArrayList<String> redeemedPromotionId = new ArrayList<String>();
    ArrayList<Date> redeemedPromotionDate = new ArrayList<>();
    TextView textViewNoPromotion;
    long oneDay = 1000 * 60 * 60 * 24;
    long today, startDay,dayLeft;
    boolean hasCommonTag = false;

    @BindView(R.id.layout_preferredClient)
    LinearLayout preferredClientLayout;

    @SuppressLint("StringFormatMatches")
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View containerView = inflater.inflate(R.layout.fragment_promotion, container, false);
        ButterKnife.bind(this,containerView);
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();

        global = Global.getInstance();
        dialog = new ProgressDialog(getActivity());
        global.recentPromotion = new ArrayList<PromotionData>();
        global.interestedPromotion = new ArrayList<PromotionData>();
        recyclerView = containerView.findViewById(R.id.recyclerview);
        textViewNoPromotion = containerView.findViewById(R.id.textViewNoPromotion);
        //work with button controller

        recent = containerView.findViewById(R.id.recent);
        interest = containerView.findViewById(R.id.interest);
        mFirebaseStore  = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        if (currentUser!= null)
        {
            user_id = currentUser.getUid();
        }
        else {
            user_id = Global.valueUserId;
        }
        Realm.init(getActivity());  // add this line
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);

        mRealm = Realm.getDefaultInstance();

        dialog.show();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mFirebaseStore.collection("users").document(currentUser.getUid()).collection("redeemed_promotions").addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (redeemedModel == null)
                {
                    global.redeemedModelArrayList  = new ArrayList<RedeemedModel>();
                    for (DocumentChange doc:queryDocumentSnapshots.getDocumentChanges()) {

                        if (doc.getType() == DocumentChange.Type.ADDED) {

                            redeemedModel = new RedeemedModel();
                            if ( doc.getDocument().get("id")!= null)
                            {
                                redeemedModel.setPromotionId( doc.getDocument().get("id").toString());
                            }   if ( doc.getDocument().get("promotion_id")!= null)
                            {
                                redeemedModel.setId( doc.getDocument().get("promotion_id").toString());
                            }
                            if ( doc.getDocument().get("user_id")!= null)
                            {
                                redeemedModel.setUserId( doc.getDocument().get("user_id").toString());
                            }
                            if ( doc.getDocument().get("date")!= null)
                            {
                                redeemedModel.setDate( (Date)doc.getDocument().get("date"));
                            }
                        }
                        global.redeemedModelArrayList.add(redeemedModel);
                    }
                    getRedeemList();
                }


            }
        });




   //     RealmResults<RedeemedModel> redeemedModels = mRealm
   //             .where(RedeemedModel.class)
    //            .findAll();
    //    global.redeemedModelArrayList.addAll(mRealm.copyFromRealm(redeemedModels));

        if (global.redeemedModelArrayList.isEmpty()) {
        }


        recent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecentButtonActive();

                //Sections
                if (!global.recentPromotion.isEmpty())
                {

                    recyclerView.setVisibility(View.VISIBLE);
                    //Your RecyclerView.Adapter
                    promotion_array = global.recentPromotion;
                    adapter= new SimpleAdapter(getActivity(),promotion_array);
                    recyclerView.setAdapter(adapter);

/*                    //This is the code to provide a sectioned list
                    List<SimpleSectionedRecyclerViewAdapter.Section> sections =
                            new ArrayList<SimpleSectionedRecyclerViewAdapter.Section>();
                    sections.add(new SimpleSectionedRecyclerViewAdapter.Section(0,"Ciriuga Estetica"));
              //      sections.add(new SimpleSectionedRecyclerViewAdapter.Section(3,"Medicina Estetica"));

                    //Add your adapter to the sectionAdapter
                    SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
                    SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new
                            SimpleSectionedRecyclerViewAdapter(getActivity(),R.layout.section,R.id.section_text,adapter);
                    mSectionedAdapter.setSections(sections.toArray(dummy));

                    //Apply this adapter to the RecyclerView
                    recyclerView.setAdapter(mSectionedAdapter);*/
                }else{
                    showAlertTextView();
                }
            }
        });
        interest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setInterestButtonActive();

                //Sections
                if (global.interestedPromotion.size()>0)
                {
                    //Inialize modeldata capacity
                    recyclerView.setVisibility(View.VISIBLE);
                    //Your RecyclerView.Adapter
                    adapter= new SimpleAdapter(getActivity(),global.interestedPromotion);
                    recyclerView.setAdapter(adapter);
                    //This is the code to provide a sectioned list
/*                    List<SimpleSectionedRecyclerViewAdapter.Section> sections =
                            new ArrayList<SimpleSectionedRecyclerViewAdapter.Section>();

                    sections.add(new SimpleSectionedRecyclerViewAdapter.Section(0,"Ciriuga Estetica"));
                    sections.add(new SimpleSectionedRecyclerViewAdapter.Section(2,"Medicina Estetica"));

                    //Add your adapter to the sectionAdapter
                    SimpleSectionedRecyclerViewAdapter.Section[] dummy = new SimpleSectionedRecyclerViewAdapter.Section[sections.size()];
                    SimpleSectionedRecyclerViewAdapter mSectionedAdapter = new
                            SimpleSectionedRecyclerViewAdapter(getActivity(),R.layout.section,R.id.section_text,adapter);
                    mSectionedAdapter.setSections(sections.toArray(dummy));

                    //Apply this adapter to the RecyclerView
                    recyclerView.setAdapter(mSectionedAdapter);*/
                } else{
                    showAlertTextView();
                }
            }
        });



        if (mFirebaseStore.collection("users") != null)
        {

            DocumentReference docRef = mFirebaseStore.collection("users").document(currentUser.getUid());
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {

                    if (documentSnapshot.exists())
                    {
                        if (documentSnapshot.get("preferred_customer_start_date")!= null)
                        {
                            Global.preferredDate=  (Date)documentSnapshot.get("preferred_customer_start_date");
                            today = System.currentTimeMillis();
                            if (Global.preferredDate != null)
                            {

                                startDay = Global.preferredDate.getTime();
                                dayLeft = 180- ((today- startDay)/oneDay);
                                if (dayLeft>0)
                                {
                                    preferredClientLayout.setVisibility(View.VISIBLE);
                                    tvBenefit.setText(getActivity().getString(R.string.have_memebership,dayLeft));

                                }else
                                    preferredClientLayout.setVisibility(View.GONE);
                            }else {
                                preferredClientLayout.setVisibility(View.GONE);

                            }
                        }else
                            preferredClientLayout.setVisibility(View.GONE);


                    }
                }
            });}


        //Apply this adapter to the RecyclerView

        return containerView;
    }
    private void getRedeemList(){

        if (!global.redeemedModelArrayList.isEmpty()){
            for (int i = 0;i<global.redeemedModelArrayList.size();i++)
            {
                redeemedPromotionDate.add(global.redeemedModelArrayList.get(i).getDate());
                redeemedPromotionId.add(global.redeemedModelArrayList.get(i).getId());
            }
        }
        if (mRealm.isClosed())
        {
            mRealm = Realm.getDefaultInstance();
        }
        RealmResults<RedeemedModel> results = mRealm.where(RedeemedModel.class).findAll();

// All changes to data must happen in a transaction
        mRealm.beginTransaction();
// Delete all matches
        results.deleteAllFromRealm();
        if (!global.redeemedModelArrayList.isEmpty())
        {
            for (int i = 0;i<global.redeemedModelArrayList.size();i++)
            {
                RedeemedModel redeemedModel = mRealm.createObject(RedeemedModel.class);
                redeemedModel.setDate(global.redeemedModelArrayList.get(i).getDate());
                redeemedModel.setUserId(global.redeemedModelArrayList.get(i).getUserId());
                redeemedModel.setId(global.redeemedModelArrayList.get(i).getId());
                redeemedModel.setPromotionId(global.redeemedModelArrayList.get(i).getPromotionId());
            }
        }
        mRealm.commitTransaction();
        getPromotionList();

    }

    private void showAlertTextView(){
        recyclerView.setVisibility(View.GONE);
        textViewNoPromotion.setVisibility(View.VISIBLE);
    }

    //make 'RECIENTES' button active when click that buton
    private  void setRecentButtonActive()
    {
        recent.setBackground(getResources().getDrawable(R.drawable.ic_rounded_button_lefttapbarselect));
        recent.setTextColor(getResources().getColor(R.color.colorWhite));
        interest.setBackground(getResources().getDrawable(R.drawable.ic_rounded_button_righttapbar));
        interest.setTextColor(getResources().getColor(R.color.colorSelect));
    }
    //make 'interesado' button active when click that buton
    private void setInterestButtonActive()
    {
        interest.setBackground(getResources().getDrawable(R.drawable.ic_rounded_button_righttapbarselect));
        interest.setTextColor(getResources().getColor(R.color.colorWhite));
        recent.setBackground(getResources().getDrawable(R.drawable.ic_rounded_button_lefttapbar));
        recent.setTextColor(getResources().getColor(R.color.colorSelect));
    }


    private void getPromotionList(){
        dialog.show();
        mFirebaseStore.collection("promotions").addSnapshotListener(new MainActivity() ,new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
          //      global.recentPromotion = new ArrayList<PromotionData>();

                for (DocumentChange doc:documentSnapshots.getDocumentChanges()) {
                    if (doc.getType() == DocumentChange.Type.ADDED)
                    {
                        Date currentTime = Calendar.getInstance().getTime();

                        PromotionModel promotionModel = new PromotionModel();

                        //promotionModel = doc.getDocument().toObject(PromotionModel.class);

                        if ((Date) doc.getDocument().get("final_date")!= null)
                        {
                            promotionModel.setFinal_date((Date) doc.getDocument().get("final_date"));
                        }
                        if (doc.getDocument().get("actual_price") != null)
                        {
                            promotionModel.setActual_price(Double.parseDouble(doc.getDocument().get("actual_price").toString()));
                        }
                        if ( doc.getDocument().get("description")!= null)
                        {
                            promotionModel.setDescription( doc.getDocument().get("description").toString());
                        }
                        if (doc.getDocument().get("discount_price")!= null)
                        {
                            promotionModel.setDiscount_price((int) Double.parseDouble(doc.getDocument().get("discount_price").toString()));
                        }
                        if (doc.getDocument().get("code")!= null)
                        {
                            promotionModel.setCode(doc.getDocument().get("code").toString());
                        }
                        if (doc.getDocument().get("id") != null)
                        {
                            promotionModel.setId(doc.getDocument().get("id").toString());
                        }
                        if (doc.getDocument().get("image_url")!= null)
                        {
                            promotionModel.setImage_url(doc.getDocument().get("image_url").toString());
                        }
                        if (doc.getDocument().get("label")!=null)
                        {
                            promotionModel.setLabel(doc.getDocument().get("label").toString());
                        }
                        if (doc.getDocument().get("start_date") != null)
                        {
                            promotionModel.setStart_date((Date) doc.getDocument().get("start_date"));
                        }
                        if (doc.getDocument().get("times_used") != null)
                        {
                            promotionModel.setTimes_used(Double.parseDouble(doc.getDocument().get("times_used").toString()));
                        }
                        if (doc.getDocument().get("title")!= null)
                        {
                            promotionModel.setTitle(doc.getDocument().get("title").toString());
                        }
                        if (doc.getDocument().get("type") != null)
                        {
                            promotionModel.setType(doc.getDocument().get("type").toString());
                        }
                        if (doc.getDocument().get("usage_count") != null)
                        {
                            promotionModel.setUsage_count(Double.parseDouble(doc.getDocument().get("usage_count").toString()));
                        }
                        if (doc.getDocument().get("usage_limit") != null)
                        {
                            promotionModel.setUsage_limit(Double.parseDouble(doc.getDocument().get("usage_limit").toString()));
                        }
                        if (doc.getDocument().get("usage_per_customer") != null)
                        {
                            promotionModel.setUsage_per_customer(Double.parseDouble(doc.getDocument().get("usage_per_customer").toString()));
                        }
                        ArrayList  promotionTags = new ArrayList();
                        if (doc.getDocument().get("tags") != null)
                        {
                            promotionTags = (ArrayList<String>) doc.getDocument().get("tags");
                        }

                        int promotionTagCount = promotionTags.size();
                        if (promotionModel.getFinal_date().after(currentTime)){
                            if (Global.tags != null && !Global.tags.isEmpty() && promotionTags.size()!= 0)
                            {
                                int userTagCount = Global.tags.size();
                                for (int i = 0;i<promotionTagCount;i++)
                                {
                                    for (int j = 0;j<userTagCount;j++){
                                        if (Global.tags.get(j).equals(promotionTags.get(i)))
                                        {
                                            hasCommonTag = true;
                                        }else if (promotionTags.get(0).equals("everyone")){
                                            hasCommonTag = true;
                                        }
                                    }
                                }
                            }
                        }

                        if (hasCommonTag)
                        {
                            global.recentPromotion.add(new PromotionData(promotionModel));
                            hasCommonTag = false;
                        }
                    }

                    }
                global.interestedPromotion = new ArrayList<PromotionData>();
                if (mRealm.isClosed())
                {
                    mRealm = Realm.getDefaultInstance();
                }
                results = mRealm.where(PromotionData.class).findAll();

                global.interestedPromotion.addAll(mRealm.copyFromRealm(results));
                if (global.recentPromotion.size() != 0)
                {
                    if (global.interestedPromotion.size() != 0){
                        for (int i = 0;i<global.recentPromotion.size();i++)
                        {
                            for (int j = 0;j<global.interestedPromotion.size();j++)
                            {
                                if (global.recentPromotion.get(i).getId()!= null &&  global.recentPromotion.get(i).getId().equals(global.interestedPromotion.get(j).getId()))
                                {
                                    global.recentPromotion.remove(i);
                                }
                                else  if(global.recentPromotion.get(i).getCode()!= null && global.recentPromotion.get(i).getCode().equals(global.interestedPromotion.get(j).getCode()))
                                {
                                    if (global.recentPromotion.get(i).getTitle() != null && global.recentPromotion.get(i).getTitle().trim().equals(global.interestedPromotion.get(j).getTitle().trim())){
                                        global.recentPromotion.remove(i);
                                    }
                                }
                            }
                        }
                    }
                }
                //Sections
                if (global.recentPromotion.size()>0)
                {
                    recyclerView.setVisibility(View.VISIBLE);
                    //Your RecyclerView.Adapter
                    adapter= new SimpleAdapter(getActivity(),global.recentPromotion);
                    recyclerView.setAdapter(adapter);
                    //This is the code to provide a sectioned list

                }else{
                    showAlertTextView();
                }
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onDestroy() {
        mRealm.close();
        super.onDestroy();

    }

    public class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.SimpleViewHolder> {

        private final Context mContext;
        private List<String> mData;
        ArrayList<PromotionData> CardArray;
        private RequestOptions requestOptions = new RequestOptions();

        //you can change UI with card_view.xml

        public class SimpleViewHolder extends RecyclerView.ViewHolder {
            // controllers
            public  CardView cardView;
            private TextView mDiscount;
            private TextView mdate;
            private RoundedImageView photo;
            private ImageView circle;
            private TextView circleText;
            private TextView offeredText;
            public SimpleViewHolder(View view) {
                super(view);
                this.cardView = view.findViewById(R.id.card_container);
                this.mDiscount = view.findViewById(R.id.text_discount);
                this.mdate = view.findViewById(R.id.text_date);
                this.photo =  view.findViewById(R.id.img_photo);
                this.circle = view.findViewById(R.id.circle);
                this.circleText = view.findViewById(R.id.percentIncircleText);
                this.cardView = view.findViewById(R.id.card_container);
                this.offeredText = view.findViewById(R.id.offered);
            }
        }

        public SimpleAdapter(Context context, ArrayList<PromotionData> data) {
            mContext = context;
            this.CardArray = data;
        }

        public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            final View view = LayoutInflater.from(mContext).inflate(R.layout.card_view, parent, false);
            return new SimpleViewHolder(view);
        }

        @Override
        public void onBindViewHolder(SimpleViewHolder holder, final int position) {

            final int n_pos  = position;
            if (this.getItemCount()>0)
            {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM"); // Set your date format
                final String currentDate = "Hasta  " + sdf.format(this.CardArray.get(position).getFinal_date());
                if (!redeemedPromotionId.contains(CardArray.get(n_pos).getId()))
                {
                    float discount_percent = (float) this.CardArray.get(n_pos).getDiscount_price()/(float)this.CardArray.get(n_pos).getActual_price() * 100;

                    holder.offeredText.setVisibility(View.GONE);
                    holder.circleText.setVisibility(View.VISIBLE);
                    // holder.photo.setImageResource(CardArray.get(position).get())
                    Glide.with(mContext).setDefaultRequestOptions(requestOptions).load(CardArray.get(n_pos).getImage_url()).into(holder.photo);

         //           Picasso.with(mContext).load(CardArray.get(n_pos).getImage_url()).placeholder(mContext.getResources().getDrawable(R.drawable.ic_default)).into(holder.photo);
                    holder.mDiscount.setText(CardArray.get(n_pos).getTitle());
                    holder.circleText.setText(this.CardArray.get(position).getLabel());
                    holder.circle.setImageResource(R.drawable.percent_high);

                    holder.mdate.setText(currentDate);

                    holder.cardView.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {
                            Global.currentPromotionData = CardArray.get(n_pos);
                            Global.position = n_pos;
                            CardDetailFragment cardDetailFragment   = new CardDetailFragment();
                            FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
                            Bundle sendData = new Bundle();
                            sendData.putString("id",CardArray.get(position).getId());
                            sendData.putString("photopath",CardArray.get(position).getImage_url());
                            sendData.putString("description",CardArray.get(position).getDescription());
                            sendData.putDouble("percentage_discount",CardArray.get(position).getDiscount_price());
                            sendData.putDouble("actual_price",CardArray.get(position).getActual_price());
                            sendData.putString("code",CardArray.get(position).getCode());
                            sendData.putString("recovery_date",currentDate);
                            sendData.putString("head",CardArray.get(position).getTitle());
                            sendData.putBoolean("isInterested",CardArray.get(position).isUserInterested());
                            cardDetailFragment.setArguments(sendData);
                            fragmentTransaction.replace(R.id.MainFrame,cardDetailFragment,null);
                            fragmentTransaction.commit();
                        }
                    });
                }
                else if (redeemedPromotionId.contains(CardArray.get(position).getId())){
                    if (redeemedPromotionDate.contains(CardArray.get(position).getFinal_date()))
                    {
                        holder.circleText.setVisibility(View.GONE);
                        holder.offeredText.setVisibility(View.VISIBLE);
                        holder.offeredText.setText(getString(R.string.offer_used).toUpperCase());
                        holder.offeredText.setTextColor(getResources().getColor(R.color.light_black));
                        holder.cardView.setCardBackgroundColor(getResources().getColor(R.color.disableColor));
                        Glide.with(mContext).setDefaultRequestOptions(requestOptions).load(CardArray.get(n_pos).getImage_url()).into(holder.photo);
                        holder.mDiscount.setText(CardArray.get(n_pos).getTitle());
                        holder.mdate.setText(currentDate);
                    }
                }

            }else {
                holder.itemView.setVisibility(View.INVISIBLE);
            }
        }


        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }

        @Override
        public int getItemCount() {
            return CardArray.size();
        }
    }

}