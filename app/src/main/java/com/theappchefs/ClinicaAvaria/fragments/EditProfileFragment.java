package com.theappchefs.ClinicaAvaria.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.theappchefs.ClinicaAvaria.Global;
import com.theappchefs.ClinicaAvaria.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class EditProfileFragment extends Fragment {

    @BindView(R.id.edittext_firstName)
    EditText editTextFirstName;

    @BindView(R.id.editText_lastName)
    EditText editTextlastName;

    @BindView(R.id.editTexT_rut)
    EditText editTextRut;

    String firstName = "";
    String lastName = "";
    String rut = "";

    @BindView(R.id.tv_changePasswords)
    TextView changePassword;

    @BindView(R.id.btn_save)
    Button btn_save;

    FirebaseAuth firebaseAuth;
    FirebaseFirestore db;
    FirebaseUser currentUser;

    ProgressDialog progressDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_edit_profile, container, false);
        ButterKnife.bind(this,containerView);


        ((AppCompatActivity)getActivity()).getSupportActionBar().show();

        progressDialog = new ProgressDialog(getActivity());

        db = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();




        return containerView;
    }

    private void initValue(){

    }

    @OnClick(R.id.btn_save)
    public void onSaveClicked(){
        firstName = editTextFirstName.getText().toString().trim();
        lastName = editTextlastName.getText().toString().trim();
        rut = editTextRut.getText().toString().trim();

        if (firstName.length() == 0 ||lastName.length() ==0||rut.length() ==0){
            Toast.makeText(getActivity(),"Please input all of fields.",Toast.LENGTH_SHORT).show();
        }else {
            String userId = "";
            if (currentUser != null)
            {
                userId = currentUser.getUid();
            }else {
                userId = Global.valueUserId;
            }
            db.collection("users").document(userId).update("first_name",firstName);
            db.collection("users").document(userId).update("last_name",lastName);
            db.collection("users").document(userId).update("rut",rut);
        }

    }

    @OnClick(R.id.tv_changePasswords)
    public void onChangePasswordClicked(){
        ChangePasswordFragment promotionFragment  = new ChangePasswordFragment();
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.MainFrame,promotionFragment,null);
        fragmentTransaction.commit();
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
