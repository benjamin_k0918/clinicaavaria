package com.theappchefs.ClinicaAvaria.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.theappchefs.ClinicaAvaria.Global;
import com.theappchefs.ClinicaAvaria.MyPreferenceManager;
import com.theappchefs.ClinicaAvaria.R;
import com.theappchefs.ClinicaAvaria.activities.MainActivity;
import com.theappchefs.ClinicaAvaria.model.CAReopSession;
import com.theappchefs.ClinicaAvaria.model.CARepoUser;

import java.util.ArrayList;
import java.util.Date;

import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.Realm;
import io.realm.RealmConfiguration;


public class IngresaFragment extends Fragment {
    FirebaseAuth firebaseAuth;
    FirebaseFirestore db;
    Realm mRealm;
    ProgressDialog dialog;
    SharedPreferences.Editor mEditor;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_ingresa, container, false);

        Button btnLogin = (Button)containerView.findViewById(R.id.btn_login);
        final TextView textWarning = (TextView) containerView.findViewById(R.id.text_warning);
        final EditText email = (EditText)containerView.findViewById(R.id.editTextemail);
        final EditText password = (EditText)containerView.findViewById(R.id.password);
        final TextView forgotPassword = (TextView)containerView.findViewById(R.id.text_forgot_password);
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());

        db = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        final FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        dialog = new ProgressDialog(getActivity());


        mRealm = Realm.getDefaultInstance();
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name =  email.getText().toString().trim();
                String pass = password.getText().toString().trim();


                if (!name.isEmpty() && !pass.isEmpty())
                {
                    dialog.setMessage("Please wait.");
                    dialog.show();
                    try{
                        firebaseAuth.signInWithEmailAndPassword(name,pass).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull final Task<AuthResult> task) {
                                if (task.isSuccessful())
                                {
                                    if (currentUser!= null)
                                    {
                                        mRealm.beginTransaction();
                                        CAReopSession.getInstance().setUserId(currentUser.getUid());
                                        CAReopSession.getInstance().setToken(currentUser.getIdToken(true).toString());
                                        mRealm.commitTransaction();

                                        if (db.collection("users") != null)
                                        {
                                            DocumentReference docRef = db.collection("users").document(currentUser.getUid());
                                            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                                @Override
                                                public void onSuccess(DocumentSnapshot documentSnapshot) {

                                                    if (documentSnapshot.exists())
                                                    {
                                                        String strLastName =  documentSnapshot.get("last_name").toString();
                                                        String strFirstName = documentSnapshot.get("first_name").toString();
                                                        String strEmail = documentSnapshot.get("email").toString();
                                                        Global.tags = (ArrayList<String>) documentSnapshot.get("tags");
                                                        if (documentSnapshot.get("preferred_customer_start_date")!= null)
                                                        {
                                                            Global.preferredDate=  (Date)documentSnapshot.get("preferred_customer_start_date");
                                                        }

                                                    }
                                                }
                                            });}}

                                    SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences(" SHARED_PREFERENCES_NAME ", android.content.Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor =      preferences.edit();
                                    editor.putString(Global.login, Global.loggedin);
                                    editor.commit();

                                    Intent intent = new Intent(getActivity(),MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                    getActivity().finish();
                                }

                                else if (!task.isSuccessful())
                                {
                                    dialog.cancel();
                                    Toast.makeText(getActivity(),task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                                    //   dialog.cancel();
                                }
                            }
                        });
                    }catch (Exception e)
                    {
                        dialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(getActivity(),"Unable to connect to the Internet.",Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    textWarning.setVisibility(View.VISIBLE);
                }
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotPasswordFragment forgotPasswordFragment  = new ForgotPasswordFragment();
                FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.MainFrame,forgotPasswordFragment,null);
                fragmentTransaction.commit();
            }
        });

        return containerView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dialog.cancel();
        mRealm.close();
    }
}
