package com.theappchefs.ClinicaAvaria.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.theappchefs.ClinicaAvaria.Global;
import com.theappchefs.ClinicaAvaria.R;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ForgotPasswordSuccessFragment extends Fragment {


    @BindView(R.id.enter_again)
    Button enterAgainButton;

    @BindView(R.id.tv_email)
    TextView tvEmail;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View containerView = inflater.inflate(R.layout.fragment_forgotpassword_succes, container, false);
        ButterKnife.bind(this,containerView);

        tvEmail.setText(Global.currentUserEmail);

        return containerView;
    }

    @OnClick(R.id.enter_again)
    public void enterAgainButton(){
        IngresaFragment ingresaFragment  = new IngresaFragment();
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.MainFrame,ingresaFragment,null);
        fragmentTransaction.commit();
    }

}
