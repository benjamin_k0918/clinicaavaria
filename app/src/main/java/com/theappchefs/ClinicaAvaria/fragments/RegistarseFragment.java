package com.theappchefs.ClinicaAvaria.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.theappchefs.ClinicaAvaria.R;
import com.theappchefs.ClinicaAvaria.model.CAReopSession;
import com.theappchefs.ClinicaAvaria.model.CARepoUser;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class RegistarseFragment extends Fragment {
    Realm mRealm;
    EditText editTextFirstName,editTextLastName;
    EditText editTextEmail;
    EditText editTextPassword,editTextConfirmPassword;
    FirebaseAuth firebaseAuth;
    FirebaseFirestore db;
    FirebaseUser currentUser;
    CARepoUser caRepoUser;
    ProgressDialog dialog;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View containerView=inflater.inflate(R.layout.fragment_registarse, container, false);

        editTextFirstName = containerView.findViewById(R.id.text_first_name);
        editTextLastName = containerView.findViewById(R.id.text_last_name);
        editTextEmail = containerView.findViewById(R.id.text_email);
        editTextPassword = containerView.findViewById(R.id.text_password);
        editTextConfirmPassword = containerView.findViewById(R.id.text_confirm_password);

        firebaseAuth = FirebaseAuth.getInstance();
        dialog = new ProgressDialog(getActivity());
        Realm.init(getActivity());  // add this line
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);


        mRealm = Realm.getDefaultInstance();

        caRepoUser = new CARepoUser();

        db = FirebaseFirestore.getInstance();

        Button button = containerView.findViewById(R.id.button_start);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String firstName = editTextFirstName.getText().toString().trim();
                final String email = editTextEmail.getText().toString().trim();
                final String password = editTextPassword.getText().toString().trim();
                final String confirmPassword = editTextConfirmPassword.getText().toString();
                final String lastName = editTextLastName.getText().toString().trim();

                if (!firstName.isEmpty() && !email.isEmpty() && !password.isEmpty() && !confirmPassword.isEmpty() && confirmPassword.equals(password)) {
                    dialog.setMessage("Please wait.");
                    dialog.show();
                    //         mRealm.beginTransaction();
                    //        caRepoUser = mRealm.createObject(CARepoUser.class);
                    caRepoUser.setFirstName(firstName);
                    caRepoUser.setLastName(lastName);
                    caRepoUser.setEmail(email);
                    //   Toast.makeText(getContext(),firstName+ lastName,Toast.LENGTH_SHORT).show();
                    try{
                        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    //  Toast.makeText(getContext(),currentUser.getUid().toString(),Toast.LENGTH_SHORT).show();
                                    final Map<String, Object> user = new HashMap<>();
                                    user.put("email", email);
                                    user.put("first_name", firstName);
                                    user.put("last_name", lastName);
                                    user.put("preferred_customer", false);
                                    JSONObject js = new JSONObject(user);
                                    DocumentReference ref = db.collection("user").document();
                                    //  final  String docId = ref.getId();
                                    currentUser = firebaseAuth.getCurrentUser();
                                    db.collection("users").document(currentUser.getUid()).set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            mRealm.beginTransaction();
                                            CAReopSession.getInstance().setUserId(currentUser.getUid());
                                            CAReopSession.getInstance().setToken(currentUser.getIdToken(true).toString());
                                            mRealm.commitTransaction();
                                            gotoRegitarseFragment();
                                        }
                                    });

                                } else {
                                    dialog.cancel();
                                    Toast.makeText(getActivity(), "Error while SignUp", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(getActivity(),"Netword status is bad or you need to install Google Play Services",Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }
            }
        });

        TextView textView = containerView.findViewById(R.id.already_account);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoForgotPasswordFragment();
            }
        });

        return containerView;
    }


    private void gotoRegitarseFragment(){
        SuccessRegisterFragment successRegisterFragment  = new SuccessRegisterFragment();
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.MainFrame,successRegisterFragment,null);
        fragmentTransaction.commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRealm.close();
        dialog.cancel();

    }

    private void gotoForgotPasswordFragment(){
        ForgotPasswordFragment forgotPasswordFragment  = new ForgotPasswordFragment();
        FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.MainFrame,forgotPasswordFragment,null);
        fragmentTransaction.commit();
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String title);
    }

}
