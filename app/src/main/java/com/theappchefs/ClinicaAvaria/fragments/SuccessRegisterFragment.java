package com.theappchefs.ClinicaAvaria.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.theappchefs.ClinicaAvaria.R;

import butterknife.ButterKnife;


public class SuccessRegisterFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View containerView = inflater.inflate(R.layout.fragment_success_register, container, false);
        ButterKnife.bind(this,containerView);

        Button button = containerView.findViewById(R.id.login);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IngresaFragment ingresaFragment  = new IngresaFragment();
                FragmentManager fragmentManager= getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction =   fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.MainFrame,ingresaFragment,null);
                fragmentTransaction.commit(); }
        });

        return containerView;
    }
    // TODO: Rename method, update argument and hook method into UI event

}
