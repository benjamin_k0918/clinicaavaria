package com.theappchefs.ClinicaAvaria.fragments;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.theappchefs.ClinicaAvaria.R;


import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AvariaFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    final  String AvariaURL = "https://clinicaavaria.cl/";

    @BindView(R.id.avaria_View)
    WebView mWebView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final   View containerView=inflater.inflate(R.layout.fragment_avaria, container, false);
        ButterKnife.bind(this,containerView);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // chromium, enable hardware acceleration
            mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        mWebView.loadUrl(AvariaURL);

        mWebView.setWebViewClient(new WebViewClient() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onLoadResource(WebView view, String url) {
                try {
                    mWebView.evaluateJavascript("document.querySelector('.header').remove();",null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return  containerView;
    }




}
