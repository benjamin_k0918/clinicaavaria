import android.view.View;

/**
 * Created by Cyhon on 3/4/2018.
 */

public interface ItemClickListener {
    void onItemClick(View v, int position);
}
