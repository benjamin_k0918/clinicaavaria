package model;

/**
 * Created by Cyhon on 3/3/2018.
 */

public class CardViewModel {

    private int id;
    private int promotion_photo_path;
    private String percentage_discount;
    private String  description;
    private String actual_price;
    private String recovery_date;
    private int circle_path;

    public CardViewModel(int ID,int PROMOTION_PHOTO_PATH,String PERCENTAGE_DISCOUNT,String DESCRIPTION,String ACTUAL_PRICE,String RECOVERY_DATE,int Circle_path)
    {
        this.id = ID;
        this.promotion_photo_path = PROMOTION_PHOTO_PATH;
        this.percentage_discount = PERCENTAGE_DISCOUNT;
        this.description = DESCRIPTION;
        this.actual_price = ACTUAL_PRICE;
        this.recovery_date = RECOVERY_DATE;
        this.circle_path = Circle_path;
    }

    public void setId(int ID){this.id = ID;}

    public void setPromotion_photo_path(int PROMOTION_PHOTO_PATH){this.promotion_photo_path = PROMOTION_PHOTO_PATH;}

    public void setPercentage_discount(String PERCENTAGE_DISCOUNT){this.percentage_discount = PERCENTAGE_DISCOUNT;}

    public void setSuggested_cost(String DESCRIPTION){this.description = DESCRIPTION;}

    public void setActual_price (String ACTUAL_PRICE){this.actual_price = ACTUAL_PRICE;}

    public void setRecovery_date(String RECOVERY_DATE){this.recovery_date = RECOVERY_DATE;}
    public void setCircle_path(int Circlepath){this.circle_path = Circlepath;}
    public int getId(){return this.id;}

    public String getPercentage_discount(){return  this.percentage_discount;}

    public int getPromotion_photo_path(){return  this.promotion_photo_path;}

    public String getDescription(){return this.description;}

    public String getActual_price(){return this.actual_price;}

    public String getRecovery_date(){return this.recovery_date;}
    public int getCircle_path(){return this.circle_path;}

}
